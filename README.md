xbox.py - reworked from source xbox.py by Steven Jacobs from https://github.com/FRC4564/Xbox
============================================================================================

Example class usage:

    import xbox
    joy = xbox.Joystick()         #Initialize joystick
    
    if joy.A():                   #Test state of the A button (1=pressed, 0=not pressed)
        print 'A button pressed'
    x_axis   = joy.leftX()        #X-axis of the left stick (values -1.0 to 1.0)
    (x,y)    = joy.leftStick()    #Returns tuple containing left X and Y axes (values -1.0 to 1.0)
    trigger  = joy.rightTrigger() #Right trigger position (values 0 to 1.0)
    
    joy.close()                   #Cleanup before exit

Note:
Running with sudo privileges to allow xboxdrv necessary control over USB devices.
If you want, you can provide your user account with the proper access, so you needn't use sudo.

First, add your user to the root group. Here's how to do this for the user ‘pi’

    sudo usermod -a -G root pi

Create a permissions file using the nano text editor.

    sudo nano /etc/udev/rules.d/55-permissions-uinput.rules

Enter the following rule and save your entry.

    KERNEL=="uinput", MODE="0660", GROUP="root"
